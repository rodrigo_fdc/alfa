<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <h3>Relatório de Movimemtação</h3>
        <ul>
            @foreach($conteiners as $conteiner)
            <li>Cliente : <strong>{{$conteiner->customer}}</strong>; Quantidade de Importação: {{$conteiner->total(1, $conteiner->customer)}}; Quantidade de Exportação: {{$conteiner->total(2, $conteiner->customer)}}</li>
            @endforeach
        </ul>
<ul>
    <li>Total de Importação: {{$importacao}}</li>
    <li>Total de Exportação: {{$exportacao}}</li>
</ul>
    </div>
</body>

</html>