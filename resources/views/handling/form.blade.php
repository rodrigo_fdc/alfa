<!doctype html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

  <title>Dashboard Template for Bootstrap</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- Custom styles for this template -->
  <link href="{{ asset('assets/css/dashboard.css')}}" rel="stylesheet">
</head>

<body>
  <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
      <li class="nav-item text-nowrap">
        <a class="nav-link" href="#">Sign out</a>
      </li>
    </ul>
  </nav>

  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="#">
                <span data-feather="home"></span>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('conteiners.index')}}">
                <span data-feather="file-text"></span>
                Conteiner
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('handling.index')}}">
                <span data-feather="file-text"></span>
                Movimentação
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
          </ul>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a class="d-flex align-items-center text-muted" href="#">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Teste
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        @if($errors->all())
          @foreach($errors->all() as $key => $value)
              <div class="alert alert-danger">
                  {{ $value }} <br>
              </div>
          @endforeach
        @endif
        <h2>Dados do Conteiner</h2>
        <form class="contact-form" action="{{route('handling.store')}}" method="POST">
            <input type="hidden" name="id" value="{{@$handling->id}}">
            @csrf
            <div class="form-group">
                <label for="movement_type">Containers</label>
                <select class="custom-select" name="conteiners_id" disabled>
                    <option value="">Selecione ...</option>
                    @foreach($conteiners as $conteiner)
                      <option value="{{$conteiner->id}}" @if(@$conteiner->id == $conteiner_id) selected @endif >{{$conteiner->nConteiner}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="movement_type">Tipo da movimentação</label>
                <select class="custom-select" name="movement_type">
                    @foreach($type_handing as $key => $value)
                    <option value="{{$key}}" @if($key == $movement_type) selected @endif>{{$value}}</option>

                    @endforeach
                </select>

            </div>
            <div class="form-group">
                <label for="type">Dia e Hora de Início da Movimentação</label>
                <input type="date" class="form-control" name="dt_start" value="{{$dt_start}}">
                <br>
                <input type="time" class="form-control" name="hr_start" value="{{$hr_start}}">
            </div>
            <div class="form-group">
                <label for="status">Dia e Hora Fim da Movimentação</label>
                <input type="date" class="form-control" name="dt_end" value="{{$dt_end}}">
                <br>
                <input type="time" class="form-control" name="hr_end" value="{{$hr_end}}">
            </div>
            <hr>
            <button type="submit" class="btn btn-success float-right">Salvar</button>
            <a href="{{ route('handling.index')}}" type="button" class="btn btn-secondary">Voltar</a>
        </form>
      </main>
    </div>
  </div>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
  </script>
  <script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
  </script>

  <!-- Icons -->
  <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
  <script>
    feather.replace()
  </script>

  <!-- Graphs -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

</body>

</html>