<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Conteiner;

class ConteinerController extends Controller
{
    public function index()
    {
        $conteiners = Conteiner::all();
        return view('conteiner.index', compact('conteiners'));
    }

    public function create()
    {
        return view('conteiner.form');
    }

    public function store(Request $request)
    {
        $request->validate([
            'customer' => 'required',
            'type' => 'required',
            'status' => 'required',
            'category' => 'required',
        ]);

        $conteiner_id = $request->id;

        if (!$conteiner_id) {

            $conteiner = new Conteiner;
            $conteiner->nConteiner = "CONT" . rand(1000000, 999999999);
            $conteiner->customer = $request->customer;
            $conteiner->type = $request->type;
            $conteiner->status = $request->status;
            $conteiner->category = $request->category;

            if ($conteiner->save()) {
                return redirect()->route('conteiners.index')->with('sucess', 'Cadastrado com sucesso.');
            } else {
                return redirect()->back()->with('danger', 'Erro ao cadastrar, tente novamente.');
            }
        } else {

            $conteiners = [
                'customer' => $request->customer,
                'category' => $request->category,
                'status' => $request->status,
                'type' => $request->type
            ];

            if (Conteiner::where('id', $request->id)->update($conteiners)) {
                return redirect()->route('conteiners.index')->with('sucess', 'Atualizado com sucesso.');
            } else {
                return redirect()->back()->with('danger', 'Erro ao alterar, tente novamente.');
            }
        }
    }

    public function update($id)
    {
        $conteiner = Conteiner::find($id);
        return view('conteiner.form', compact('conteiner'));
    }

    public function destroy($id)
    {
        if (Conteiner::where('id', $id)->delete()) {
            return redirect()->route('conteiner.index')->with('sucess', 'Deletado com sucesso.');
        } else {
            return redirect()->back()->with('danger', 'Erro ao deletar, tente novamente.');
        }
    }
}
