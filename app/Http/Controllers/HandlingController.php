<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Handling;
use App\Models\Conteiner;
use App\Models\ConteinerHanding;

class HandlingController extends Controller
{
    public function index()
    {
        $handlings = Handling::all();
        return view('handling.index', compact('handlings'));
    }

    public function create()
    {
        $type_handing = [
            '1' => 'Embarque',
            '2' => 'Descarga',
            '3' => 'Gate In',
            '4' => 'Gate out',
            '5' => 'Posicionamento',
            '6' => 'Pilha',
            '7' => 'Pesagem',
            '8' => 'Scanner'
        ];
        $conteiners = Conteiner::all();
        return view('handling.form', compact('conteiners', 'type_handing'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'conteiners_id' => 'required',
            'movement_type' => 'required',
            'dt_start' => 'required',
            'hr_start' => 'required',
            'dt_end' => 'required',
            'hr_end' => 'required'
        ]);
        $handling_id = $request->id;

        if (!$handling_id) {

            $handing = new Handling;
            $handing->typeMoviment = $request->movement_type;
            $handing->dtStart = $request->dt_start . " " . $request->hr_start;
            $handing->dtEnd = $request->dt_end . " " . $request->hr_end;

            if ($handing->save()) {

                $conteinerHanding = new ConteinerHanding;
                $conteinerHanding->handings_id = $handing->id;
                $conteinerHanding->conteiners_id = $request->conteiners_id;
                $conteinerHanding->save();

                return redirect()->route('handling.index')->with('sucess', 'Cadastrado com sucesso.');
            } else {
                return redirect()->back()->with('danger', 'Erro ao cadastrar, tente novamente.');
            }
        } else {

            $conteiners = [
                'customer' => $request->customer,
                'category' => $request->category,
                'status' => $request->status,
                'type' => $request->type
            ];

            if (Conteiner::where('id', $request->id)->update($conteiners)) {
                return redirect()->route('handling.index')->with('sucess', 'Atualizado com sucesso.');
            } else {
                return redirect()->back()->with('danger', 'Erro ao alterar, tente novamente.');
            }
        }
    }

    public function update($id)
    {
        $type_handing = [
            '1' => 'Embarque',
            '2' => 'Descarga',
            '3' => 'Gate In',
            '4' => 'Gate out',
            '5' => 'Posicionamento',
            '6' => 'Pilha',
            '7' => 'Pesagem',
            '8' => 'Scanner'
        ];

        $handling = Handling::find($id);

        $conteinerHandlings = ConteinerHanding::where('handings_id', $id)->get();
        $conteiners = Conteiner::all();

        $conteiner_id = $conteinerHandlings[0]->conteiners_id;

        $dateHoraStart = explode(" ", $handling->dtStart);
        $dateHoraEnd = explode(" ", $handling->dtEnd);

        $movement_type = $handling->typeMoviment;
        $dt_start = $dateHoraStart[0];
        $dt_end = $dateHoraEnd[0];

        $hr_start = $dateHoraStart[1];
        $hr_end = $dateHoraEnd[1];

        return view(
            'handling.form',
            compact('handling', 'type_handing', 'conteiners', 'conteiner_id', 'movement_type', 'dt_start', 'dt_end', 'hr_start', 'hr_end')
        );
    }

    public function destroy($id)
    {
        if (Handling::where('id', $id)->delete()) {
            return redirect()->route('handling.index')->with('sucess', 'Deletado com sucesso.');
        } else {
            return redirect()->back()->with('danger', 'Erro ao deletar, tente novamente.');
        }
    }

    public function report(){
        $conteiners = Conteiner::select('customer')->distinct()->get();
        $importacao = Conteiner::where('category', 1)->count();
        $exportacao = Conteiner::where('category', 2)->count();

        return view('handling.report', compact('conteiners', 'importacao', 'exportacao'));
    }
}
