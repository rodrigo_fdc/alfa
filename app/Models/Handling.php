<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Handling extends Model
{
    use HasFactory;

    public function type($id)
    {
        $type_handing = [
            '1' => 'Embarque',
            '2' => 'Descarga',
            '3' => 'Gate In',
            '4' => 'Gate out',
            '5' => 'Posicionamento',
            '6' => 'Pilha',
            '7' => 'Pesagem',
            '8' => 'Scanner'
        ];

        foreach ($type_handing as $key => $value) {
            switch ($id) {
                case $key:
                    return $value;
                    break;
            }
        }
    }
}
