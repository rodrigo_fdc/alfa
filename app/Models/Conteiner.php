<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Conteiner extends Model
{
    use HasFactory;

    public function total($attr, $customer)
    {
        return DB::table('conteiners')->where('category', $attr)->where('customer', $customer)->count();
    }
    public function group()
    {

        // return DB::table('conteiners')->select('customer')->distinct()->get();
        return DB::table('conteiners')->select('category')->distinct()->get();
    }
}
