<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ConteinerController;
use App\Http\Controllers\HandlingController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('conteiners',              [ConteinerController::class, 'index'])->name('conteiners.index');
Route::get('conteiners/create',       [ConteinerController::class, 'create'])->name('conteiners.create');
Route::get('conteiners/update/{id}',  [ConteinerController::class, 'update'])->name('conteiners.update');
Route::get('conteiners/destroy/{id}', [ConteinerController::class, 'destroy'])->name('conteiners.destroy');
Route::post('conteiners/store',       [ConteinerController::class, 'store'])->name('conteiners.store');

Route::get('handling',                [HandlingController::class, 'index'])->name('handling.index');
Route::get('handling/create',         [HandlingController::class, 'create'])->name('handling.create');
Route::get('handling/report',         [HandlingController::class, 'report'])->name('handling.report');
Route::post('handling/store',         [HandlingController::class, 'store'])->name('handling.store');
Route::any('handling/update/{id}',   [HandlingController::class, 'update'])->name('handling.update');
Route::any('handling/destroy/{id}',   [HandlingController::class, 'destroy'])->name('handling.destroy');