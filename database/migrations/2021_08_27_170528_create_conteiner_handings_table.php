<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConteinerHandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conteiner_handings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            
            $table->bigInteger('conteiners_id')->unsigned()->index()->nullable();
            $table->foreign('conteiners_id')->references('id')->on('conteiners')->onDelete('cascade');

            $table->bigInteger('handlings_id')->unsigned()->index()->nullable();
            $table->foreign('handlings_id')->references('id')->on('handlings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conteiner_handings');
    }
}
